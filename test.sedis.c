/*
https://gitlab.com/denisdemais/sedis
2024 denis dos santos silva
*/

#define MINCTEST_EXTRA
#include "minctest.h"           // https://github.com/codeplea/minctest

#include "sedis.h"

const char* initial_sql;
const char* initial_pragma;

char *buf;
const char *buffer;
int rc;
int blen;
int c;
int e;


void lsetup() {
	initial_sql = NULL;
	initial_pragma = NULL;
	buf = NULL;
	buffer = NULL;
	blen = 0;
	c = e = 0;
}


void lteardown() {
	if (buf) free(buf);
}


void test_f5f90f062f14() {
	rc = sedis_status();
	lok(rc == SEDIS_STATUS_CLOSED);

	rc = sedis_init(initial_sql, initial_pragma);
	lok(rc == SEDIS_OK);

	rc = sedis_status();
	lok(rc == SEDIS_STATUS_OPEN);

	rc = sedis_close();
	lok(rc == SEDIS_OK);

	rc = sedis_status();
	lok(rc == SEDIS_STATUS_CLOSED);
}


void test_4a0fa7700eaf() {
	rc = sedis_init(initial_sql, initial_pragma);
	lok(rc == SEDIS_OK);

	rc = sedis_set("k", "v");
	lok(rc == SEDIS_OK);

	buffer = sedis_get("k", &blen);
	lok(buffer != NULL);
	lok(blen != 0);

	buffer = sedis_get("k'''kk", &blen);
	lok(buffer == NULL);
	lok(strcmp("near \"kk\": syntax error", sedis_error(&c, &e)) == 0);
	lok(c == SQLITE_ERROR);
	lok(e == SQLITE_ERROR);

	buffer = sedis_get("kkk", &blen);
	lok(buffer == NULL);
	lnegative(blen);

	/*
	int pCurrent;
	int pHighwater;

	pCurrent = pHighwater = 0;
	sqlite3_status(SQLITE_STATUS_MEMORY_USED, &pCurrent, &pHighwater, 0);
	printf("sqlite3_status: %d / %d", pCurrent, pHighwater);
	*/

	rc = sedis_close();
	lok(rc == SEDIS_OK);
}



int main() {
	system("clear");

	printf("\n%s\n%s\n\n", __FILE__, __DATE__ " " __TIME__);

	lrun("test init/close of library", test_f5f90f062f14);
	lrun("test set/get workflow", test_4a0fa7700eaf);

	lresults();
	return lfails != 0;
}
