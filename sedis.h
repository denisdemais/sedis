/*
https://gitlab.com/denisdemais/sedis
2024 denis dos santos silva
*/

#ifndef SEDIS_H_
#define SEDIS_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <assert.h>

extern char* strdup(const char *str);

#include "sqlite3.h"

#define SEDIS_STATUS_OPEN	(0)
#define SEDIS_STATUS_CLOSED	(1)
#define SEDIS_OK			(0)
#define SEDIS_ERROR			(-1)

#ifndef SEDIS_BUFFER
#define SEDIS_BUFFER 1024
#endif

int sedis_init(const char* initial_sql, const char* initial_pragma);
int sedis_set(const char* key, const char *value);
const char* sedis_get(const char* key, int *value_len);
const char* sedis_error(int *code, int *extended);
int sedis_status(void);
int sedis_close(void);
#endif


#ifdef SEDIS_IMPLEMENTATION
static int  sedis_state = SEDIS_STATUS_CLOSED;
static      sqlite3* sedis_base = NULL;
static      sqlite3_stmt* sedis_stmt = NULL;
static char sedis_sql[SEDIS_BUFFER];


int sedis_init(const char* initial_sql, const char* initial_pragma) {
	int rc = 0;
	
	if (sedis_state == SEDIS_STATUS_OPEN)
		return SEDIS_OK;

	// https://www.sqlite.org/inmemorydb.html
	//rc |= sqlite3_open(":memory:?cache=shared", &sedis_base);	// 298.101504ms
	//rc |= sqlite3_open(":memory:", &sedis_base);				// 0.143872ms
	rc |= sqlite3_open(":memory:", &sedis_base);
	if (rc != SQLITE_OK)
		return SEDIS_ERROR;

	sqlite3_snprintf(
		sizeof(sedis_sql),
		sedis_sql,
		"CREATE TABLE sedis(key text COLLATE BINARY UNIQUE, value text);"
		"PRAGMA shrink_memory;"
	);

	rc |= sqlite3_exec(sedis_base, sedis_sql, 0, 0, 0);

	if (initial_pragma)
		rc |= sqlite3_exec(sedis_base, sedis_sql, 0, 0, 0);

	if (initial_sql)
		rc |= sqlite3_exec(sedis_base, sedis_sql, 0, 0, 0);

	sedis_state = SEDIS_STATUS_OPEN;

	return SEDIS_OK;
}


int sedis_status(void) {
	return sedis_state;
}

int sedis_close(void) {
	if (sedis_state == SEDIS_STATUS_CLOSED)
		return SEDIS_ERROR;

    if (sedis_stmt)
		sqlite3_finalize(sedis_stmt);

	if (sedis_base)
		sqlite3_close_v2(sedis_base);

	sedis_stmt = NULL;
	sedis_base = NULL;

	sedis_state = SEDIS_STATUS_CLOSED;
	return SEDIS_OK;
}


int sedis_set(const char* key, const char* value) {
	int rc = 0;

	if (sedis_state == SEDIS_STATUS_CLOSED)
		return SEDIS_ERROR;

	if (!key || !value)
		return SEDIS_ERROR;

	// TODO: usar prepare/bind para usar 'blob'

	sqlite3_snprintf(
		sizeof(sedis_sql),
		sedis_sql,
		"insert or replace into sedis(key, value) values (%Q, %Q)",
		key,
		value
	);

	rc |= sqlite3_reset(sedis_stmt);
	rc |= sqlite3_exec(sedis_base, sedis_sql, 0, 0, 0);

	return (rc == SQLITE_OK) ? SEDIS_OK : SEDIS_ERROR;
}


const char* sedis_error(int *code, int *extended) {
	// https://www.sqlite.org/rescode.html#extrc
	if (sedis_state == SEDIS_STATUS_CLOSED) {
		*code = -1;
		*extended = -1;
		return NULL;
	}
	
	*code = sqlite3_errcode(sedis_base);
	*extended = sqlite3_extended_errcode(sedis_base);
	return sqlite3_errmsg(sedis_base);
}


const char* sedis_get(const char* key, int *outlen) {
	int rc = 0;
	*outlen = -1;

	if (sedis_state == SEDIS_STATUS_CLOSED)
		return NULL;

	if (!key)
		return NULL;

	sqlite3_snprintf(
		sizeof(sedis_sql),
		sedis_sql,
		"SELECT value FROM sedis WHERE key='%s' LIMIT 1",
		key
	);

	// TODO: usar prepare/bind para usar 'blob'

	rc = 0;
    rc |= sqlite3_reset(sedis_stmt);
    rc |= sqlite3_prepare_v2(sedis_base, sedis_sql, -1, &sedis_stmt, 0);

	// error
    if (rc != SQLITE_OK) {
		*outlen = -2;
		return NULL;
	}
	
	// SQLITE_API const unsigned char *sqlite3_column_text(sqlite3_stmt*, int iCol);

    rc = sqlite3_step(sedis_stmt);
    if (rc == SQLITE_ROW) {
		// found
		*outlen = sqlite3_column_bytes(sedis_stmt, 0);
		return (const char*) sqlite3_column_text(sedis_stmt, 0);
    } else if (rc == SQLITE_DONE) {
		// not found
		*outlen = -3;
		return NULL;
    } else {
		// error
		*outlen = rc;
		return NULL;
    }
}

#endif

